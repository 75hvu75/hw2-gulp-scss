import gulp from 'gulp';
import fileInclude from 'gulp-file-include';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import clean from 'gulp-clean';
import cleanCSS from 'gulp-clean-css';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import imagemin from 'gulp-imagemin';
import autoprefixer from 'gulp-autoprefixer';
import { create as bsCreate } from 'browser-sync';
import purgeCSS from 'gulp-purgecss';
import fsExtra from 'fs-extra';

const browserSync = bsCreate();
const sass = gulpSass(dartSass);

const paths = {
    src: {
        html: 'index.html',
        scss: 'src/scss/style.scss',
        js: 'src/js/app.js',
        images: 'src/img/**/*.{jpg,jpeg,png,gif,webp}',
        svgicons: 'src/svgicons/*.svg',
        fonts: 'src/fonts/**/*.*',
    },
    dist: {
        html: 'dist',
        css: 'dist/css',
        js: 'dist/js',
        images: 'dist/img',
        svgicons: 'dist/svgicons',
        fonts: 'dist/fonts',
    },
    watch: {
        html: 'src/**/*.html',
        scss: 'src/scss/**/*.scss',
        js: 'src/js/**/*.js',
        images: 'src/img/**/*.{jpg,jpeg,png,gif,webp}',
        svgicons: 'src/svgicons/*.svg',
        fonts: 'src/fonts/**/*.*',
    },
    clean: './dist',
};

// Очищення папки dist
function reset() {
    return gulp.src(paths.clean, { allowEmpty: true }).pipe(clean());
}

// Компіляція SCSS у CSS
function scss() {
    return gulp
        .src(paths.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(
            autoprefixer({
                grid: true,
                overrideBrowserslist: ['last 3 version'],
                cascade: true,
            })
        )
        .pipe(gulp.dest(paths.dist.css)) // не стислий дубль файлу стилів style.css
        .pipe(concat('styles.min.css'))
        .pipe(cleanCSS())
        .pipe(
            purgeCSS({
                content: ['dist/*.html'],
                safelist: ['open'], // Залишити клас .open
            })
        )
        .on('end', async () => {
            // Видалення style.css після виконання purgeCSS
            fsExtra.unlink(`${paths.dist.css}/style.css`);
        })
        .pipe(gulp.dest(paths.dist.css))
        .pipe(browserSync.stream());
}

// Конкатенація та мініфікація JS файлів
function js() {
    return gulp.src(paths.src.js).pipe(concat('scripts.min.js')).pipe(uglify()).pipe(gulp.dest(paths.dist.js)).pipe(browserSync.stream());
}

// Оптимізація зображень
function images() {
    return gulp
        .src(paths.src.images)
        .pipe(
            imagemin({
                progressive: true,
                svgoPlugins: [{ removeViewBox: false }],
                interlaced: true,
                optimizationLevel: 3,
            })
        )
        .pipe(gulp.dest(paths.dist.images))
        .pipe(browserSync.stream());
}

// Копіювання SVG-іконок
function svgIcons() {
    return gulp.src(paths.src.svgicons).pipe(gulp.dest(paths.dist.svgicons)).pipe(browserSync.stream());
}

// Копіювання шрифтів
function fonts() {
    return gulp.src(paths.src.fonts).pipe(gulp.dest(paths.dist.fonts)).pipe(browserSync.stream());
}

// Копіювання HTML файлу у папку dist
function html() {
    return gulp
        .src(paths.src.html)
        .pipe(
            fileInclude({
                prefix: '@@',
                basepath: '@root',
            })
        )
        .pipe(gulp.dest(paths.dist.html))
        .pipe(browserSync.stream());
}

// Запуск сервера та відстеження змін файлів
function server() {
    browserSync.init({
        server: {
            baseDir: paths.dist.html,
        },
        notify: false,
        port: 3000,
    });
}

// Спостерігач за змінами в файлах
function watcher() {
    gulp.watch(paths.watch.html, html);
    gulp.watch(paths.watch.scss, scss);
    gulp.watch(paths.watch.js, js);
    gulp.watch(paths.watch.images, images);
    gulp.watch(paths.watch.svgicons, svgIcons);
    gulp.watch(paths.watch.fonts, fonts);
}

// Основні робочі завдання
const mainTasks = gulp.series(fonts, gulp.parallel(html, scss, js, images, svgIcons));

gulp.task('dev', gulp.series(reset, mainTasks, gulp.parallel(watcher, server)));
gulp.task('build', gulp.series(reset, mainTasks));

// Запуск робочого завдання за замовчуванням
gulp.task('default', gulp.series('dev'));
