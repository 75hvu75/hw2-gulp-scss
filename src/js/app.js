'use strict';

const hamBtn = document.querySelector('.hamburger');
const menu = document.querySelector('.menu');

hamBtn.addEventListener('click', () => {
    menu.classList.toggle('open');
});

document.addEventListener('click', event => {
    const targetElement = event.target;
    if (!targetElement.closest('.open') && !targetElement.closest('.hamburger')) {
        menu.classList.remove('open');
    }
});
